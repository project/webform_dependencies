<?php

/**
 * @file
 * This module uses the functionality offered by the form_dependencies.module to provide the
 * functionality to webform components.
 *
 */

/**
 * Function that travels recursively through the webform components and the $form array to find
 * and prepare dependency fields.
 * @param object $node The webform node object.
 * @param array $form Referenced form array.
 * @param int $parent Optional parent cid. That's the component id for the dependent field.
 */
function _webform_dependencies_travel($node, &$form, $parent = 0) {
  foreach ($node->webform['components'] as $cid => $component) {
    $key = $component['form_key'];
    if ($component['pid'] == $parent && isset($form[$key])) {
      if (!empty($component['dependency']['dep']) && $dependency = _webform_dependencies_trace_parents($node->webform['components'], $component['dependency']['dep'])) {

        $dep_component = $node->webform['components'][$component['dependency']['dep']];
        if ($dep_component['type'] == 'select' && $dep_component['extra']['multiple']) {

        }

        $form[$key]['#dependency'] = $dependency;
        $form[$key]['#dependency_cid'] = $cid;
        $form[$key]['#dependency_dep'] = $component['dependency']['dep'];
      }
      _webform_dependencies_travel($node, $form[$key], $cid);
    }
  }
}

/**
 * Function for creating a #parents array for a given component id (parent).
 */
function _webform_dependencies_trace_parents($components, $pid = 0) {
  $parents = array();
  while ($pid > 0 && isset($components[$pid])) {
    $parents[] = $components[$pid]['form_key'];
    $pid = $components[$pid]['pid'];
  }
  if ($parents) {
    $parents[] = 'submitted';
    return array_reverse($parents);
  }
  return FALSE;
}

/**
 * Implementation of hook_form_alter().
 */
function webform_dependencies_form_alter($form_id, &$form) {

  if (substr($form_id, 0, 20) == 'webform_client_form_' && isset($form['#parameters'][1]->nid)) {
    $node = $form['#parameters'][1];

    if ($node->webform['dependencies'] && $node->webform['components']) {
      $form['#dependencies'] = TRUE;

      _webform_dependencies_travel($node, $form['submitted']);
    }

  }
  elseif ($form_id == 'webform_components_form' && isset($form['#node'], $form['#node']->webform['components'])) {
    // This can be used later on to alter the main form and show dependencies there as well.
  }
  elseif ($form_id == 'webform_component_edit_form' && isset($form['#parameters'][1]->nid)) {

    // Alter weights so that we can place a fieldset just above the advanced element.
    $form['advanced']['#weight'] = 9;
    $form['submit']['#weight'] = 10;

    $cid = $form['cid']['#value'];
    $node = $form['#parameters'][1];
    $component = $node->webform['components'][$cid];

    if (!in_array($component['type'], array('pagebreak', 'fieldset', 'markup'))) {

      $form['#submit']['webform_dependencies_form_submit'] = $component;

      $form['dependency'] = array(
        '#type' => 'fieldset',
        '#title' => t('Dependency'),
        '#collapsible' => TRUE,
        '#collapsed' => !(isset($component['dependency']['dep']) && $component['dependency']['dep']),
        '#tree' => TRUE,
      );

      $options = array('' => t('- No dependency -'));

      foreach ($node->webform['components'] as $dep_cid => $dep_component) {
        if (!in_array($dep_component['type'], array('pagebreak', 'fieldset', 'markup')) && $dep_cid != $cid) {
          $options[$dep_cid] = $dep_component['name'] .' ('. $dep_component['type'] .')';
        }
      }

      $form['dependency']['dep'] = array(
        '#type' => 'select',
        '#title' => t('Dependency'),
        '#description' => t('Select the field this component is depending on.'),
        '#options' => $options,
        '#default_value' => $component['dependency']['dep'],
      );
    }
  }
}

/**
 * Submit handling function for the webform component form.
 * @todo Extend the submit handling with value checks etc...
 */
function webform_dependencies_form_submit($form_id, $form_values) {
  db_query("DELETE FROM {webform_dependencies} WHERE nid = %d AND cid = %d", $form_values['nid'], $form_values['cid']);
  if (!empty($form_values['dependency']) && !empty($form_values['dependency']['dep'])) {
    db_query("INSERT INTO {webform_dependencies} (nid, cid, dep) VALUES (%d, %d, %d)", $form_values['nid'], $form_values['cid'], $form_values['dependency']['dep']);
  }
}

/**
 * Function for retrieving dependencies.
 */
function webform_dependencies_load($nid) {
  $result = db_query("SELECT * FROM {webform_dependencies} WHERE nid = %d", $nid);
  $dependencies = array();
  while ($record = db_fetch_array($result)) {
    $cid = $record['cid'];
    unset($record['nid']);
    unset($record['cid']);

    // Unserialize the data.
    $record['data'] = $record['data'] ? @unserialize($record['data']) : array();

    // Store it in a new array.
    $dependencies[$cid] = $record;
  }

  return $dependencies;
}

/**
 * Implementation of hook_nodeapi().
 */
function webform_dependencies_nodeapi(&$node, $op, $arg3 = NULL, $arg4 = NULL) {
  if ($op == 'load' && $node->type == 'webform') {
    if ($node->webform && $node->webform['components']) {

      // Load the dependencies.
      $deps = webform_dependencies_load($node->nid);
      if ($deps) $node->webform['dependencies'] = TRUE;

      // Merge the dependencies into the components array.
      foreach ($deps as $cid => $dependency) {
        if (isset($node->webform['components'][$cid])) {
          $node->webform['components'][$cid]['dependency'] = $dependency;
        }
      }
    }
  }
}